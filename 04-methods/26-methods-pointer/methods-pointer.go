package main

import "fmt"


type ByteSlice []byte

// En Efective GO lo proponen así
func (p *ByteSlice) Append(data []byte) {
	slice := *p
	slice = append(slice, data...)
	*p = slice
}

// Mas sencillo desde mi punto de vista
func (p *ByteSlice) Append2(data []byte) {
	*p = append(*p, data...)
}

// En Efective GO lo proponen así
// satisfies the standard interface io.Writer
func (p *ByteSlice) Append3(data []byte) (n int, err error) {
	slice := *p
	slice = append(slice, data...)
	*p = slice
	return len(*p), nil
}

func (p *ByteSlice) Append4(data []byte) (n int, err error) {
	*p = append(*p, data...)
	return len(*p), nil
}

func main() {
	a := ByteSlice([]byte{1, 2, 3})
	b := []byte{4, 5, 6}
	a.Append(b)
	fmt.Println("RESULTADO DEL APPEND:", a)

	c := ByteSlice([]byte{1, 2, 3})
	d := []byte{4, 5, 6}
	c.Append2(d)
	fmt.Println("RESULTADO DEL APPEND2:", c)

	e := ByteSlice([]byte{1, 2, 3})
	f := []byte{4, 5, 6}
	e.Append2(f)
	fmt.Println("RESULTADO DEL APPEND3:",e)

	g := ByteSlice([]byte{1, 2, 3})
	h := []byte{4, 5, 6}
	g.Append2(h)
	fmt.Println("RESULTADO DEL APPEND4:", g)	

	i := ByteSlice([]byte{1, 2, 3})
	j := []byte{4, 5, 6}
	(&i).Append2(j)
	fmt.Println("RESULTADO DEL APPEND4:", (&i))	
}