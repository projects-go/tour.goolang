package main

import "net/http"
import "fmt"
import "log"

type Counter struct {
	n int
}

// The receiver needs to be a pointer so the increment is visible to the caller
func (ctr *Counter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctr.n++
	fmt.Fprintf(w, "counter = %d\n", ctr.n)
}

func main() {
	ctr := new(Counter)	
	http.Handle("/counter", ctr)
	log.Fatal(http.ListenAndServe(":8080", nil))	
}