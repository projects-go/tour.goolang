package main

import "sort"
import "fmt"

type Sequence []int

func (s Sequence) Len() int {
	return len(s)
}


func (s Sequence) Less(x, y int) bool {
	return x < y
}

func (s Sequence) Swap(x, y int) {
	s[x], s[y] = s[y], s[x]
}

func (s Sequence) String() string {
	sort.Sort(s)
	str := "["
	for i, elem := range s {
		if i > 0 {
			str += " " 
		}
		str += fmt.Sprint(elem)
	}
	return str + "]"
}

// The String method of Sequence (before method) is recreating the work that Sprint already does for slices. 
func (s Sequence) String2() string {
	sort.Sort(s)
	return fmt.Sprint([]int(s))
}

func (s Sequence) String3() string {
	sort.IntSlice(s).Sort()
	return fmt.Sprint([]int(s))
}
func main() {
	var s Sequence
	s = []int{6, 5, 3, 8, 1, 2}
	fmt.Println("SALIDA: ", s.String())
	fmt.Println("SALIDA2: ", s.String2())
	fmt.Println("SALIDA2: ", s.String3())
}