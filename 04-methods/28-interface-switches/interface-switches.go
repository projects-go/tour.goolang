package main 

import "fmt"

type Stringer interface {
	String() string
}

type Stronger interface {
	String() string
	ObtieneEntero() int
}

type MyType struct {
	S string
	I int
}

func (s MyType) String() string {
	return s.S
}

func (s MyType) ObtieneEntero() int {
	return s.I
}

func getString(i interface{}) string {
	switch str:= i.(type) {
	case string:
		return str
	case Stringer:
		return str.String()
	case Stronger:
		return str.String()
	}
	return ""
}

func assert() string {
	var i interface{} = "hello arnaldo"
	v, ok := i.(string)
	if ok {
		return v
	} else {
		return "" 
	}
}

func assert2() string {
	var i interface{} = "hello arnaldo"
	v, ok := i.(string)
	if ok {
		return v
	} else {
		return "" 
	}
}

func main() {
	//Empty interface (may hold values of any type)
	// Empty interfaces are used by code that handles values of unknown type.
	var value interface{} = "hola"
	fmt.Println("Resultado:", getString(value))
	var value2 interface{} = 3
	fmt.Println("Resultado:", getString(value2))
	var value3 interface{} = MyType{"Arnaldo", 2}
	fmt.Println("Resultado:", getString(value3))
	value4 := MyType{"Arnaldo", 23}
	fmt.Println("Resultado:", value4.ObtieneEntero())
	fmt.Println("Type assertion:", assert())
}