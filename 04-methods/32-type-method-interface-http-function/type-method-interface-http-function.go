// https://golang.org/pkg/net/http/#Handle
// https://golang.org/pkg/net/http/#HandlerFunc

package main

import "net/http"
import "fmt"
import "log"
import "os"

// Argument server.
func ArgServer(w http.ResponseWriter, req *http.Request) {
	fmt.Println("W:", w, "ARGS:", os.Args)
	fmt.Fprintln(w, os.Args)
}

func main() {
    // func Handle(pattern string, handler Handler)
	// Handle registers the handler for the given pattern in the DefaultServeMux.
	
	// The HandlerFunc type is an adapter to allow the use of ordinary functions
	// as HTTP handlers. If f is a function with the appropriate signature, 
	// HandlerFunc(f) is a Handler that calls f.
	http.Handle("/args", http.HandlerFunc(ArgServer))
	log.Fatal(http.ListenAndServe(":8080", nil))	
}