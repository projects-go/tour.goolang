package main

import "net/http"
import "fmt"
import "log"

// But why make Counter a struct? An integer is all that's needed.
type Counter int

// The receiver needs to be a pointer so the increment is visible to the caller
func (ctr *Counter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	*ctr++
	fmt.Fprintf(w, "counter = %d\n", *ctr)
}

func main() {
	ctr := new(Counter)
	http.Handle("/counter", ctr)
	log.Fatal(http.ListenAndServe(":8080", nil))	
}