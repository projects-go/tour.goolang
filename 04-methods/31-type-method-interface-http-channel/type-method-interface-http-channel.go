package main

import "net/http"
import "fmt"
import "log"

// A channel that sends a notification on each visit.
// (Probably want the channel to be buffered.)
type Chan chan *http.Request

func (ch Chan) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ch <- req
	fmt.Fprint(w, "notification sent")
}

func main() {
	ch := new(Chan)
	http.Handle("/counter", ch)
	log.Fatal(http.ListenAndServe(":8080", nil))	
}