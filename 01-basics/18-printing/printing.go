package main

import "fmt"

type cadena string
func (s cadena) String() string {
	panic("bad")
}

func main() {
	var nombre string = "Hola Arnaldo"
	var edad int = 33
	var verdad bool = true
	var myfloat = 1.0
	var inter interface{} = 23
	type Datos struct {
		nombre string
		edad int
	}
	misDatos := Datos{"Leo", 34}
	puntero := &misDatos

	fmt.Println("************ PRINTF ************")

	fmt.Println("GENERAL")
	fmt.Printf("Printf %%v. El valor en un formato por defecto. Sin nombres de campo: %v\n", misDatos)
	fmt.Printf("Printf %%v. Interface con valor entero: %#v. Tipo: %T\n", inter, inter)
	fmt.Printf("Printf %%+v. Añade nombres de campos al anterior: %+v\n", misDatos)
	fmt.Printf("Printf %%#v. Representación del valor: %#v\n", misDatos)
	fmt.Printf("Printf %%T. Tipo del valor. Edad: %T\n", edad)

	fmt.Println("\nBOOLEAN")
	fmt.Printf("Printf %%t. Devuelve true o false. Verdad: %t\n", verdad)

	fmt.Println("\nINTEGER")
	fmt.Printf("Printf %%b. Con base 2. Edad: %b\n", edad)
	fmt.Printf("Printf %%c. El caracter representado por el correspondiente código Unicode. Edad: %c\n", edad)
	fmt.Printf("Printf %%d. Con base 10. Edad: %d\n", edad)
	fmt.Printf("Printf %%o. Con base 8. Edad: %o\n", edad)

	fmt.Println("\nFLOATING-POINT AND COMPLEX CONSTITUENTS")
	fmt.Printf("Printf %%f. Punto decimal, pero sin exponente. Ancho predeterminado, precisión predeterminada. myFloat: %f\n", myfloat)
	fmt.Printf("Printf %%9f. Ancho 9, precisión predeterminada. myFloat: %f\n", myfloat)
	fmt.Printf("Printf %%.9f. Ancho predeterminado, precisión 9. myFloat: %f\n", myfloat)
	fmt.Printf("Printf %%9.f. Ancho 9, precisión 0. myFloat: %f\n", myfloat)
	fmt.Printf("Printf %%9.9f. Ancho 9, precisión 9. myFloat: %f\n", myfloat)

	fmt.Println("\nSTRING AND SLICES OF BYTES")
	fmt.Printf("Printf %%s. Los bytes no interpretados de la cadena o porción. Nombre: %s\n", nombre)
	fmt.Printf("Printf %%q. Una cadena de comillas dobles escapada de forma segura con la sintaxis de Go. Nombre: %q\n", nombre)
	fmt.Printf("Printf %%x. Base 16, minúsculas, dos caracteres por byte. Nombre: %x\n", nombre)
	fmt.Printf("Printf %%X. Base 16, mayúscula, dos caracteres por byte. Nombre: %X\n", nombre)

	fmt.Println("\nPOINTER")
	fmt.Printf("Printf %%p. Notación base 16, con una ventaja de 0x. Puntero a misDatos: %p\n", puntero)
	fmt.Printf("Printf %%d. Puntero a misDatos: %d\n", &puntero)

	fmt.Println("************ SPRINTF ************")
	fmt.Sprintf("%[2]d %[1]d\n", 11, 22)
	fmt.Println(fmt.Sprintf("Sprintf %%[2]d %%[1]d. %[2]d %[1]d\n", 11, 22))
	fmt.Println(fmt.Sprintf("%[3]*.[2]*[1]f", 12.0, 2, 6))
	
	fmt.Println("************ PANIC ************")	
	cadena.String("Arnaldo")
}

