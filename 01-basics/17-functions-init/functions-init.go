package main

import "fmt"
import "log"
import "flag"

var user, home, gopath string = "arnaldo", "", ""

func init() {
	if user == "" {
		log.Fatal("Usuario no establecido")
	}
	if home == "" {
		home = "/home/" + user
	}
	if gopath == "" {
		gopath = home + "/go"
	}
	// gopath may be overridden by --gopath flag on command line. (NO SE COMO HACERLO FUNCIONAR)
	flag.StringVar(&gopath, "gopath", gopath, "override default GOPATH")
	fmt.Println("USER:", user)
	fmt.Println("HOME:", home)
	fmt.Println("GOPATH:", gopath)
}

func main() {
}