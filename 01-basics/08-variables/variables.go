package main

import(
	"fmt"
	"os"
)

var c, python, java bool

func main() {
	var i int
	fmt.Println(i, c, python, java)

	//MIS PRUEBAS
	fmt.Println("\n------- MY TESTS -------")

	fmt.Println("\nPRUEBA: ")
	var (
		home = os.Getenv("HOME")
		user = os.Getenv("USER")
		gopath = os.Getenv("GOPATH")
	)
	
	fmt.Println("HOME:", home, "\nUSER:", user, "\nGOPATH:", gopath)
}