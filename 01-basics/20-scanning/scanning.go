package main

import "fmt"

func main() {
    var X int
    var Y int
 
    fmt.Printf("\nIntital X: %d, Y: %d", X, Y)
	// Sscan escanea la cadena de argumentos, almacenando los sucesivos valores
	// separados por espacios en argumentos sucesivos. Los Newlines cuentan como espacio.
    fmt.Sscan("100\n200", &X, &Y)
    fmt.Printf("\nSscan X: %d, Y: %d", X, Y)
 
	// Sscanf escanea la cadena de argumentos, almacenando los sucesivos valores
	// separados por espacios en argumentos sucesivos según lo determinado por el formato.
    fmt.Sscanf("(10, 20)", "(%d, %d)", &X, &Y)
    fmt.Printf("\nSscanf X: %d, Y: %d", X, Y)
	 
	// Sscanln es similar a Sscan, pero detiene el escaneo en una nueva línea
    fmt.Sscanln("50\n50", &X, &Y)
    fmt.Printf("\nSscanln X: %d, Y: %d", X, Y)
}