/**
 * VER El ULTIMO APPEND, PORQUE LA LONGITUD ES 5 Y LA CAPACIDAD 6. PARA MI DEBERÍA SER 5 AMBOS

 * It is common to append new elements to a slice, and so Go provides a built-in append function. 
 * func append(s []T, vs ...T) []T
 * The first parameter s of append is a slice of type T, 
 * and the rest are T values to append to the slice.
 * The resulting value of append is a slice containing all the elements of the original slice plus 
 * the provided values.
 * If the backing array of s is too small to fit all the given values a bigger array will be allocated. 
 * The returned slice will point to the newly allocated array.
 */

package main 

import "fmt"


func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func main() {
	var s []int
	printSlice(s)

	// append works on nil slices.
	s = append(s, 1)
	printSlice(s)

	// The slice grows as needed.
	s = append(s, 1)
	printSlice(s)

	// We can add more than one element at a time.
	s = append(s, 2, 3, 4)
	printSlice(s)		

	//MIS PRUEBAS
	fmt.Println("\n------- MY TESTS -------")

	fmt.Println("\nPRUEBA: Anexar elementos a un slice")
	a := []int{1,2,3}
	a = append(a, 4, 5, 6)
	fmt.Println("Append in x:", a)
	fmt.Println("\nPRUEBA: Anexar un slice a otro slice")
	x := []int{1,2,3}
	y := []int{4,5,6}
	x = append(x, y...)
	fmt.Println("Append in x", x)


}