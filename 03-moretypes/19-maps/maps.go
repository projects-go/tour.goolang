/**
 * A map maps keys to values.
 * The zero value of a map is nil. A nil map has no keys, nor can keys be added.
 * The make function returns a map of the given type, initialized and ready for use.
 */

package main 

import "fmt"
import "log"

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex

func cambiarValor(mapa map[string]string, clave, valor string){
	mapa[clave] = valor
}
func main() {
	m = make(map[string]Vertex)
	m["Bell Labs"] = Vertex {
		40.68433, -74.39967,
	}
	fmt.Println(m["Bell Labs"])

	//MIS PRUEBAS
	fmt.Println("\n------- MY TESTS -------")

	fmt.Println("\nPRUEBA: Pasar un mapa a una función y cambiar el contenido")
	var mymap = map[string]string{
		"nombre": "Arnaldo",
		"apellido": "Ceballos",
		"dni": "30298963",
	}
	cambiarValor(mymap,"nombre","Raúl")
	fmt.Println(mymap)

	fmt.Println("\nPRUEBA: Comma OK")
	if value, ok := mymap["estado_civil"]; ok {
		fmt.Printf("Existe el campo estado_civil!! y el valor es %v\n", value)
	} else {
		log.Println("Desconocido estado_civil")
	}

	if value, ok := mymap["apellido"]; ok {
		fmt.Printf("Existe el campo apellido!! y el valor es %v\n", value)
	} else {
		log.Println("Desconocido apellido")
	}

}