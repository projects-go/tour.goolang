//VER EL DROP

/**
 * A slice has both a length and a capacity.
 * Length len(s): the number of elements it contains.
 * Capacity cap(s): the number of elements in the underlying array, counting from the first element in the slice.
 * You can extend a slice's length by re-slicing it, provided it has sufficient capacity.
 */

package main 

import "fmt"

func printSlice(s []int) {
	fmt.Printf("%v - LENGTH: %d CAPACITY: %d\n", s, len(s), cap(s))
}

func Append(slice, data []int) []int {
	lenSlice := len(slice)
	if lenSlice + len(data) > cap(slice){
		newSlice :=make([]int, (lenSlice + len(data))*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[:lenSlice + len(data)]
	copy(slice[lenSlice:], data)
	return slice
}

func AppendWithoutControl(slice, data []int) []int {
	lenSlice := len(slice)
	slice = slice[:lenSlice + len(data)]
	copy(slice[lenSlice:], data)
	return slice
}

func main() {
	s := []int{2, 3, 5, 7, 11, 13}
	printSlice(s)

	// Slice the slice to give it zero length.
	s = s[:0]
	printSlice(s)

	// Extend its length.
	s = s[:4]
	printSlice(s)

	// Drop its first two values.
	s = s[2:]
	printSlice(s)

	//MIS PRUEBAS
	fmt.Println("\n------- MY TESTS -------")

	fmt.Println("\nPRUEBA: Crear slice con longitud menor a capacidad")
	a := make([]int, 3, 5)
	fmt.Print("RESULTADO) ")
	printSlice(a)

	fmt.Println("\nPRUEBA: Crear slice con longitud igual a capacidad")
	b := make([]int, 3)
	fmt.Print("RESULTADO) ")
	printSlice(b)

	myarray := [5]int{1,2,3,4,5}
	fmt.Println("\nUNDERLYING ARRAY:", myarray, "- LENGTH:", len(myarray), "- CAPACITY:", cap(myarray))
	elements := myarray[1:3]
	fmt.Print("SLICE ELEMENTS: ")
	printSlice(elements)

	fmt.Println("\nPRUEBA: Comparte almacenamiento con el arreglo subyacente")
	fmt.Println("1) UNDERLYING ARRAY:", myarray)
	fmt.Println("2) ASIGNO NUEVO VALOR A POSICION 1 EN SLICE ELEMENTS")
	elements[1] = 33
	fmt.Println("RESULTADO) SLICE ELEMENTS:", elements)
	fmt.Println("RESULTADO) UNDERLYING ARRAY:", myarray, "--> CAMBIA EL ARREGLO SUBYACENTE")
	
	fmt.Println("\nPRUEBA: The length of a slice may be changed as long as it still fits within the limits of the underlying array")
	elements = elements[:len(myarray)-2]
	fmt.Print("SLICE ELEMENTS: ")
	printSlice(elements)
	fmt.Println("1) APPEND CON CONTROL DE LONGITUD")
	elements = Append(elements, []int{5,6,7})
	fmt.Print("RESULTADO) ")
	printSlice(elements)
	fmt.Println("2) APPEND SIN CONTROL DE LONGITUD")
	elements = AppendWithoutControl(elements, []int{8,9,10,11,12,13,14})
	fmt.Print("RESULTADO) ")
	printSlice(elements)
}
