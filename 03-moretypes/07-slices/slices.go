/**
 * Slice is a dynamically-sized
 * The type []T is a slice with elements of type T.
 * A slice is formed by specifying two indices, a low and high bound, separated by a colon
 * Includes the first element, but excludes the last one.
 * First element in position 0
 */
package main 

import "fmt"

func main() {
	primes := [6]int{2, 3, 5, 7, 11, 13}
	var s []int = primes[1:4]
	fmt.Println(s)
}