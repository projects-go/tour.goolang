package main

import "time"
import "fmt"

func Announce(message string, delay time.Duration) {
	go func() {
		time.Sleep(delay)
		fmt.Println(message)
	}()
}

func main() {
	Announce("Hola Arnaldo", 0)
	for i :=0; i < 40; i++ {
		fmt.Println("Hola")
	}
}